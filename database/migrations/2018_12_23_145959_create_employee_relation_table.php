<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeeRelationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employee_relation', function (Blueprint $table) {
            $table->increments('id');
			
			$table->integer('employee_id')->unsigned();
			$table->foreign('employee_id')->references('id')->on('employee');
  
			$table->integer('firm_id');
			$table->integer('branch_id');
            $table->timestamps();
        });
    }

    /** 
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employee_relation');
    }
}
