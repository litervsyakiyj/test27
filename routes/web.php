<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () { return view('welcome'); }); 
Route::get('/firms', 'FirmController@all'); 
Route::get('/employees', 'EmployeeController@all'); 

Route::get('/search', 'SearchController@index');

Route::get('/redis', function() {
	  
	//App\Jobs\SendMessage::dispatchNow("TEST MESSAGE");
	App\Jobs\SendMessage::dispatch("TEST MESSAGE");
	
}); 
 
//создание, редактирвоание объектов 
Route::get('/admin/new_firm/', 'admin\FirmController@new_firm');
Route::get('/admin/edit_firm/{id}', 'admin\FirmController@edit_firm');
Route::get('/admin/new_branch/', 'admin\FirmController@new_branch');
Route::get('/admin/edit_branch/{id}', 'admin\FirmController@edit_branch');
Route::get('/admin/new_employee/', 'admin\EmployeeController@new_employee');
Route::get('/admin/edit_employee/{id}', 'admin\EmployeeController@edit_employee');
	
//
Route::get('/admin/create_firm', ['as' => 'create_firm', 'uses' => 'admin\FirmController@create']); 
Route::get('/admin/update_firm/{id}', 'admin\FirmController@update');
Route::get('/admin/create_branch', ['as' => 'create_firm', 'uses' => 'admin\FirmController@create2']); 
Route::get('/admin/update_branch/{id}', 'admin\FirmController@update2');
Route::get('/admin/create_employee', ['as' => 'create_employee', 'uses' => 'admin\EmployeeController@create']); 
Route::get('/admin/update_employee/{id}', 'admin\EmployeeController@update');

//
Route::get('/api/branch/{id}', 'admin\FirmController@branch_for_firm');