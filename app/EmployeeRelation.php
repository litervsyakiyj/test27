<?php

namespace App; 

use Illuminate\Database\Eloquent\Model;
use App\Employee;

class EmployeeRelation extends Model
{
    //protected $table = "employee_relation";
    
    //
    protected $fillable = [
        'employee_id', 'firm_id', 'branch_id'
    ];
	
	public function employees() { 
		return $this->belongsTo('App\Employee');
	}
}
