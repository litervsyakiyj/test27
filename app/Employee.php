<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\EmployeeRelation;

class Employee extends Model
{
    //protected $table = "employee";
	
    //
    protected $fillable = [
        'id', 'name'
    ];
	
	public function employee_relation() {  
		return $this->hasMany('App\EmployeeRelation', 'employee_id', 'id');
	}
}
