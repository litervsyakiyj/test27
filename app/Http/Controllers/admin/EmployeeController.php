<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Employee;
use App\EmployeeRelation;
use App\Firm;

class EmployeeController extends Controller
{
    //
    public function new_employee() {		
        $firm = Employee::where('status', 1)->get();
				
		return view('admin.new_firm', compact('firm'));
	}
	
    public function edit_employee($id) {		
        $firm = Employee::find($id);
		
        return view('admin.edit_employee', compact('firm'));
	} 
	 
    public function create(Request $request) {
		 
        try {
			$input = $request->all();  
        } catch (\Exception $e) {
            //return redirect()->back()->withInput()->withError(trans('message.error'));
			return redirect()->back();
        }
		  
        $firm2 = Employee::create($input);  
		//$firm2->employee_relation()->create($input);
		
		foreach($input['branch'] as $branch_id) {
			$input['branch_id'] = $branch_id;   
			$firm2->employee_relation()->create($input);
		} 
	} 
	
    public function update($id, Request $request) { 
		$input = $request->all();  
	
        $firm = Employee::where('id', $id)->first();
        $firm->update($input);
		
		return redirect()->back();
	} 
	
    public function destroy($id) {
        $project = Employee::withTrashed()->where('id',$id)->first();
        
        $project->forceDelete();
    
        return redirect()->route('admin.papers.index');
    }
}
