<html>
    <head>
        <title>App Name - @yield('title')</title>
		<!-- Bootstrap core CSS -->
		<link href="https://getbootstrap.com/docs/4.0/dist/css/bootstrap.min.css" rel="stylesheet">
		<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
		<style>
		.select2-container {		
			display: block;
			width: 100%!important; 
		}
		.select2-container--default .select2-selection--single {
			display: block;
			width: 100%!important;
			height: auto!important;
			padding: .375rem .75rem;
			font-size: 1rem;
			line-height: 1.5;
			color: #495057;
			background-color: #fff;
			background-clip: padding-box;
			border: 1px solid #ced4da;
			border-radius: .25rem;
			transition: border-color .15s ease-in-out,box-shadow .15s ease-in-out;	
		}
		</style>
    </head>
    <body> 
        <div class="container">
            @yield('content')
        </div>
		
		<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/css/select2.min.css" rel="stylesheet" />
		<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
		<script>
		$(document).ready(function() { 
		
		var $eventSelect2 = $(".js-example-basic-single2"),
			$eventSelect = $(".js-example-basic-single"),
			$eventSelect3 = $(".js-example-basic-multiple");
			 
		$eventSelect.select2(); 
		$eventSelect3.select2(); 

		$eventSelect.on("change", function (e) {  
		 
		//destroy 
		//$eventSelect2.select2().select2("destroy");
		//$eventSelect2.select2().val(null).trigger("change");
			
		$.getJSON( "/api/branch/" + $(this).val(), function(data) { //console.log(data);
			$eventSelect3.select2({
				data:data      
			}); 
		}); 
		 
		});  
		});  
		</script>
		
    </body>
</html>