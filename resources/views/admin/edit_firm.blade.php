@extends('master')
@section('title', 'Page Title')
@section('content')
<form action="/admin/update_firm/{{ $firm -> id }}" method="get">
  <div class="form-group">
    <label for="exampleInputEmail1">Name</label>
    <input type="text" name="name" class="form-control" id="exampleInputEmail1" value="{{ $firm -> name }}" placeholder="Enter name"> 
  </div>
  <div class="form-group">
    <label for="exampleInputEmail1">Phone</label>
    <input type="text" name="phone" class="form-control" id="exampleInputEmail1" value="{{ $firm -> phone }}" placeholder="Enter phone"> 
  </div>
  <div class="form-group">
    <label for="exampleInputEmail1">Description</label>
    <input type="text" name="description" class="form-control" id="exampleInputEmail1" value="{{ $firm -> description }}" placeholder="Enter description"> 
  </div>
  <div class="form-group">
    <label for="exampleInputEmail1">Status</label>
    <input type="text" name="status" class="form-control" id="exampleInputEmail1" value="{{ $firm -> status }}" placeholder="Enter status"> 
  </div>
  <button type="submit" class="btn btn-primary">Submit</button> 
</form>
@stop 
