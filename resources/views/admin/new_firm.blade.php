@extends('master')
@section('title', 'Page Title')
@section('content')
 
<style>
.tabs .tab-item .tab-content {display:none;}
.tabs .tab-item > input:checked ~ .tab-content {display:block;}
.tabs .tab-item input[type="radio"] {display: none;}
</style> 
 
<div class="btn btn-primary"><label for="first">Добавить новую фирму</label></div>
<div class="btn btn-primary"><label for="fourth">Добавить новый филлиал</label></div>
<div class="btn btn-primary"><label for="third">Добавить нового сотрудника</label></div>
<div class="btn btn-primary"><label for="second">Филлиалы, активных фирм</label></div>
<div class="btn btn-primary"><label for="fifth">Список сотрудников</label></div>

<div class="tabs">
<div class="tab-item">
<input type="radio" name="tabs" id="first" checked />
<div class="tab-content">
<form action="/admin/create_firm" method="get">
  <input type="hidden" name="name" class="form-control" id="firm" value="1"> 
  <div class="form-group">
    <label for="exampleInputEmail1">Name</label>
    <input type="text" name="name" class="form-control" id="exampleInputEmail1" placeholder="Enter name"> 
  </div>
  <div class="form-group">
    <label for="exampleInputEmail1">Phone</label>
    <input type="text" name="phone" class="form-control" id="exampleInputEmail1" placeholder="Enter phone"> 
  </div>
  <div class="form-group">
    <label for="exampleInputEmail1">Description</label>
    <input type="text" name="description" class="form-control" id="exampleInputEmail1" placeholder="Enter description"> 
  </div>
  <button type="submit" class="btn btn-primary">Submit</button> 
</form>
</div>
</div>
<div class="tab-item">
<input type="radio" name="tabs" id="fourth" />
<div class="tab-content">
<form action="/admin/create_branch" method="get">
  <input type="hidden" name="name" class="form-control" id="firm" value="0"> 
  <div class="form-group">
    <label for="exampleInputEmail1">Name</label>
    <input type="text" name="name" class="form-control" id="exampleInputEmail1" placeholder="Enter name"> 
  </div> 
   <div class="form-group">
  <label for="exampleInputEmail1">Address</label>
    <input type="text" name="address" class="form-control" id="exampleInputEmail1" placeholder="Enter name"> 
  </div>
  <div class="form-group">
    <label for="exampleInputEmail1">Фирма</label>
	<select class="form-control js-example-basic-single2" name="firm_id">
	@foreach ($firms as $f)
		<option value="{{ $f->id }}">{{ $f->name }}</option> 
	@endforeach
	</select>
  </div> 
  <button type="submit" class="btn btn-primary">Submit</button> 
</form>
</div>
</div>
<div class="tab-item">
<input type="radio" name="tabs" id="third" />
<div class="tab-content">
<form action="/admin/create_employee" method="get">
  <input type="hidden" name="employee_id" value="{{ $f->id }}"> 
  <div class="form-group">
    <label for="exampleInputEmail1">Name</label>
    <input type="text" name="name" class="form-control" id="exampleInputEmail1" placeholder="Enter name"> 
  </div>
  <div class="form-group">
    <label for="exampleInputEmail1">Фирма</label>
	<select class="form-control js-example-basic-single" name="firm_id">
		<option value="0"></option> 
	@foreach ($firms as $f)
		<option value="{{ $f->id }}">{{ $f->name }}</option> 
	@endforeach
	</select>
  </div> 
  <div class="form-group">
    <label for="exampleInputEmail1">Филлиал</label>
	<select class="form-control js-example-basic-multiple" name="branch[]" multiple="multiple">

	</select>
  </div> 
  <button type="submit" class="btn btn-primary">Submit</button> 
</form>
</div>
</div>
<div class="tab-item">
<input type="radio" name="tabs" id="second" />
<div class="tab-content">
<table class="table">
  <thead>
    <tr>
      <th scope="col">#</th> 
      <th scope="col">Name</th>
      <th scope="col">Phone</th>
      <th scope="col">Address</th>
      <th scope="col">Actions</th>
    </tr>
  </thead>
  <tbody>
@foreach ($firm_relation as $f)
    <tr>
      <th scope="row">{{ $f->id }}</th> 
      <td>{{ $f->name }}</td>
      <td>{{ $f->phone }}</td>
      <td>{{ $f->address }}</td>
      <td><a href="/admin/edit_branch/{{ $f->id }}">редактировать</a></td>
    </tr>
@endforeach
  </tbody>
</table>
</div>
</div>
<div class="tab-item">
<input type="radio" name="tabs" id="fifth" />
<div class="tab-content">
<table class="table">
  <thead>
    <tr>
      <th scope="col">#</th> 
      <th scope="col">Name</th>
      <th scope="col">Phone</th>
      <th scope="col">Fillial</th>
      <th scope="col">Actions</th>
    </tr>
  </thead>
  <tbody>
@foreach ($employee_relation as $f)
    <tr>
      <th scope="row">{{ $f->id }}</th> 
      <td>{{ $f->name }}</td>
      <td>{{ $f->phone }}</td>
      <td>{{ $f->branch_name }}</td>
      <td><a href="/admin/edit_firm/{{ $f->id }}">редактировать</a></td>
    </tr>
@endforeach
  </tbody>
</table>
</div>
</div>
</div>
@stop
