@extends('master')
@section('title', 'Page Title')
@section('content')
<form action="/admin/update_branch/{{ $firm -> id }}" method="get">
  <div class="form-group">
    <label for="exampleInputEmail1">Name</label>
    <input type="text" name="name" class="form-control" id="exampleInputEmail1" value="{{ $firm -> name }}" placeholder="Enter name"> 
  </div> 
  <button type="submit" class="btn btn-primary">Submit</button>
</form>
@stop
